import express from 'express';
import {
 
  signin,
  signup,
  signupAdmin
  
} from "../controllers/user.js";

const router = express.Router();
router.post("/signin",signin);
router.post("/signup", signup);
router.post("/signupAdmin", signupAdmin);

export default router;
